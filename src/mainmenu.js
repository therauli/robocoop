// MainMenu stage
var RoboCoop = RoboCoop || {};
var background;
var startButton;
var creditsButton;

RoboCoop.MainMenu = function(game) {
};

RoboCoop.MainMenu.prototype = {
    preload: function() {
        console.log('MainMenu - preload');
        this.load.image('background', 'gfx/aloitusikkuna.png');
        this.load.spritesheet('credits', 'gfx/credits_nappi.png', 120, 78);
        this.load.spritesheet('button', 'gfx/start_nappi.png', 322, 111);
        this.load.audio('titleMusic', ['sfx/RoboCoop_menu_music.mp3', 'sfx/RoboCoop_menu_music.ogg']);
        //  Load the Google WebFont Loader script
    	this.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
    },        
    create: function() {
        console.log('MainMenu - create');  
        background = this.add.sprite(0, 0, "background");        
        creditsButton = this.add.button( 1024 - 140, 30, 'credits', this.showCredits, this, 0, 0, 1);     
        startButton = this.add.button( 1024 / 2 - 150 ,  720 / 2 + 171, 'button', this.startStoryboard, this, 1, 0, 2);
		this.music = this.add.audio('titleMusic', 1, true, true);
   		this.music.play();
    },
    startStoryboard: function() {
    	console.log('MainMenu - startGame');
    	this.music.stop();
    	this.state.start('Story');        
    },
    showCredits: function() {
    	console.log('MainMenu - showCredits');
    	this.music.stop();
    	this.state.start('Credits');
    }
};
