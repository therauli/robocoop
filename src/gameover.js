var RoboCoop = RoboCoop || {};

RoboCoop.GameOver = function(game) {

};

RoboCoop.GameOver.prototype = {
	preload: function() {
		console.log('GameOver - preload');
		this.load.image('levelFailed', 'gfx/level_failed.png');
		this.load.audio('gameOver', ['sfx/game_over_alarm.mp3', 'sfx/game_over_alarm.mp3']);
	},
	create: function() {
		this.gameFail = this.add.audio('gameOver');
		this.add.sprite(200, 150, "levelFailed"); 
		this.gameFail.play();
		this.time.events.add(Phaser.Timer.SECOND*6, this.startMainMenu, this);
	},
	startMainMenu: function() {
		this.state.start('MainMenu');	
	}
};
