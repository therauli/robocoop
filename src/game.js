RoboCoop.Game = function(game) {
};

RoboCoop.Game.prototype = {
    preload: function() {
        this.load.image('background', 'gfx/tausta.png');
        this.load.image('timerbar', 'gfx/ajastin_palkki.png');
        this.load.image('victory_screen', 'gfx/level_completed.png');
        this.load.image('over_screen', 'gfx/level_failed.png');
        this.load.image('text', 'gfx/ohje_teksti.png');
        this.load.spritesheet('robot', 'gfx/Robo2Walk.png', 60, 60);
        this.load.spritesheet('tileset1', 'gfx/tilesetti2.png', 60, 60);
        this.load.spritesheet('fixed', 'gfx/korjaus_animaatio.png', 40, 40);
        this.load.spritesheet('weld', 'gfx/hitsaus_animaatio.png', 40, 40);
        this.load.spritesheet('pelaaja', 'gfx/pelaaja.png', 60, 60);

        this.load.audio('robo1Walk', ['sfx/move_legs.mp3', 'sfx/move_legs.ogg']);
        this.load.audio('roboWeld', ['sfx/welder.mp3', 'sfx/welder.ogg']);
        this.load.audio('roboExecute', ['sfx/robot_executing.mp3', 'sfx/robot_executing.ogg']);
        this.load.audio('gameMusic', ['sfx/RoboCoop_ambient_music.mp3', 'sfx/RoboCoop_ambient_music.ogg'])
        this.load.audio('buttonClick', ['sfx/UI_click.mp3', 'sfx/UI_click.ogg']);
        this.load.audio('dragRelease', ['sfx/UI_drop_to_stack.mp3', 'sfx/UI_drop_to_stack.ogg']);
        this.load.audio('bonk', ['sfx/robot_hit_wall.mp3', 'sfx/robot_hit_wall.ogg']);
        this.load.audio('nop', ['sfx/robot_no_operation.mp3', 'sfx/robot_no_operation.ogg']);
        
        for (var i = 1; i <= 6; i += 1) {
            this.load.image('symboli' + i, 'gfx/symboli' + i + '.png');
            this.load.audio('commandAudio' + i, 'sfx/robot_symbol' + i + '.mp3');
        }
        this.load.spritesheet('execute', 'gfx/execute_nappi.png', 170, 68);
        this.load.spritesheet('clear', 'gfx/clear_nappi.png', 170, 68);
        
        this.load.spritesheet('bonk', 'gfx/robo_tormaa2.png', 40, 40);

        this.buttonClick = this.add.audio('buttonClick');
        this.dragRelease = this.add.audio('dragRelease');        
    },

   	butPress: function() {
   		this.tiling.steppedButton(this);
   	},

   	winning: function() {
   		console.log("You win");
   		this.gameWon = true;
   		this.music.stop();
   		this.time.events.add(Phaser.Timer.SECOND * 1, this.winScr, this);
   	},

   	winScr: function() {
        this.state.start('GameWin');
   	},

   	weld: function() {
   		this.tiling.weld(this, robot.gridPos.x, robot.gridPos.y);
   	},

    create: function() {
    	this.gameFailed = false;
    	this.gameWon = false;
        gridTop = 23;
        gridLeft = 1024 - 640 + 20; // width - gridwidth + gridborder

        this.timerbar = this.add.sprite(105, 645, 'timerbar');
        this.add.sprite(105, 655, 'text');

        var bg = this.add.sprite(0, 0, 'background');
        bg.z = -100;


        this.music = this.add.audio('gameMusic', 0.25, true, true);
        this.music.play();

        // Add buttons
        this.add.button(11, 576, 'execute', this.execute, this, 0, 0, 1).z = -50;
        this.add.button(208, 576, 'clear', this.clear, this, 0, 0, 1).z = -50;


   		sBatch = this.add.spriteBatch();

    	this.tiling = new Tiling();
    	this.tiling.backGround(this, sBatch);
    	this.tiling.level(this, 1, sBatch);

        var x = 5;
        var y = 8;
        grid = gridToPix(x,y);
        robot = new Robot(this, grid.x, grid.y);
		robot.gridPosition(x,y);
        commands = shuffle(["move left", "move right", "move up", "move down", "nop", "weld"]);

        // Add buttons
        this.add.button(11, 576, 'execute', this.execute, this, 0, 0, 1);
        this.add.button(208, 576, 'clear', this.clear, this, 0, 0, 1);

        this.symbolsGroup = this.add.group();

        this.player = this.add.sprite((60 * 1) + gridLeft, gridTop + (60 * 5), 'pelaaja');
        this.player.angle = 90;


        this.clear();

    },
    
    clearButtonHandler: function() {
        this.buttonClick.play();
        this.clear();
    },

    gameOver: function() {
        console.log('GAME OVER!');
        this.music.stop();
        this.state.start('GameOver');
    },

    clear: function() {
         this.symbolsGroup.removeAll();

         commandStack = [undefined, undefined, undefined, undefined, undefined];

         for (var i = 0; i < 6; i += 1) {
            this.createSymbol(i);   
         }
         robot.resetCommands();       
 
    },

    execute: function() {
        this.buttonClick.play();
        robot.executeCommands();
    },


    update: function() {
        var isDone = robot.update();
        var elapsed = this.time.elapsed;
        this.timerbar.x -= elapsed / 200;

        if (this.timerbar.x < -723 && !this.gameFailed) {
            this.gameOver();
            this.gameFailed = true;
        }

        if (this.gameWon == true && (this.player.x <= (1024))) {
        	this.player.x += 7;
        }
   },

    createSymbol: function(i) {
        var bbrect = new Phaser.Rectangle(0, 0, 1024 - 640, 720);
        var symbol = this.make.sprite(23 + 60 * i, 23, 'symboli' + (i + 1));
        symbol.inputEnabled = true;
        symbol.input.enableDrag(true, false, false, 255, bbrect);
        
        this.symbolsGroup.add(symbol);

        symbol.events.onDragStart.add(this.onDragStart, this);
        symbol.events.onDragStop.add(this.onDragStop, this);

        symbol.name = i; 
        return symbol;
    },

    onDragStart: function(sprite, pointer) {
        var symbol = this.createSymbol(sprite.name);  

        sprite.scale.x = 1.2;
        sprite.scale.y = 1.2;
        
        sprite.bringToTop();
        this.buttonClick.play();
    },

    onDragStop: function(sprite, pointer) {
        var index = sprite.name;
        var validrect = new Phaser.Rectangle(20, 420, 1024 - 640, 720 - 420);
        if (validrect.contains(pointer.x, pointer.y)) {
            this.dragRelease.play();
            robot.addCommand(commands[index]);

            for (var i = 0; i < commandStack.length; i += 1) {
                if (commandStack[i] === undefined) {
                    sprite.x = 15 + 73 * i;
                    sprite.y = 418;
                    commandStack[i] = sprite;
                    sprite.inputEnabled = false;
                    sprite.events.onDragStart.removeAll();
                    sprite.events.onDragStop.removeAll()
                    break;
                }
            }
            if (i < 5) {
                return;
            }
        }
        this.symbolsGroup.remove(sprite);
    },


};
 
function gridToPix(x, y) {
    return {x: gridLeft + x * 60, y: gridTop + y * 60};
}
