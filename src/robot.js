Robot = function(game, x, y) {
    this.commandStack = [];
    this.commandAudioPlayQueue = [];
    this.game = game;
    this.walkSfx = game.add.audio('robo1Walk');
    this.weldSfx = game.add.audio('roboWeld');
    this.roboExecuteSfx = game.add.audio('roboExecute');
    this.bonkSfx = game.add.audio('bonk');
    this.nopSfx = game.add.audio('nop');
    var commandList = ["move up", "move down", "move left", "move right", "weld", "nop"];
    this.commandAudioMap = {};

    for(var i = 0; i < 6 ; i += 1)
    {
        this.commandAudioMap[commandList[i]] = game.add.audio('commandAudio' + (i + 1) );
    }
  
    this.sprite = game.add.sprite(x, y, "robot");
    this.sprite.animations.add('walk');
    game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.z = 10;
    this.target = {x: NaN,
                   y: NaN};

    this.gridPos = {x: NaN,
                    y: NaN};

    this.pauseTimer = 0;
    
    this.bonk = this.game.make.sprite(0, 0, 'bonk');
    var anim = this.bonk.animations.add('bonk');
    anim.onComplete.add(this.bonkStopped, this);


    this.done = false;
}

Robot.prototype.bonkStopped = function() {
    this.sprite.removeChild(this.bonk);
    this.game.time.events.add(Phaser.Timer.SECOND, this.executeNext, this);
}

Robot.prototype.gridPosition = function(xPos,yPos) {
	this.gridPos.x = xPos;
	this.gridPos.y = yPos;
}

Robot.prototype.executeCommands = function() {
    this.roboExecuteSfx.play();

    if (this.commandStack.length <= 0) {
        return;
    }
    this.roboExecuteSfx.onStop.add( this.playCommandAudio, this );
}

Robot.prototype.playCommandAudio = function()
{
 
    if( this.commandAudioPlayQueue.length > 0 )
    {        
        var clip = this.commandAudioPlayQueue.splice(0, 1)[0];
        clip.play();
        clip.onStop.add(this.playCommandAudio, this );
    }
    else
    {
        this.executeNext();
        this.done = false;
    }
}

Robot.prototype.executeNext = function() {
    var command = this.commandStack.splice(0, 1)[0];

    if (command === undefined || command === "") {
        this.done = true;
        this.game.clear();
        return;
    }
    var predicative = command.split(" ")[0];
    var subject = command.split(" ")[1];

    if (predicative === "move") {
        this.move(subject);
        return;
    }

    if (predicative === "nop") {
        this.nopSfx.play();
        this.game.time.events.add(Phaser.Timer.SECOND*2, this.executeNext, this);
        return;       
    }

    if (predicative === "weld") {
        this.game.time.events.add(Phaser.Timer.SECOND*3, this.executeNext, this);
        var weld = this.game.make.sprite(7, 7, 'weld');
        weld.animations.add('weld');
        this.sprite.addChild(weld);
        weld.animations.play('weld', 5, false);
        this.weldSfx.play();
        this.game.weld();
        return;        
    }
}

Robot.prototype.addCommand = function(command) {
    this.commandStack.push(command);
    this.commandAudioPlayQueue.push(this.commandAudioMap[command]);    
}

Robot.prototype.resetCommands = function() {
    this.commandStack = [];
    this.commandAudioPlayQueue = [];
}

Robot.prototype.move = function(dir) {

    var dx = 0, 
        dy = 0;
    var speed = 60;
    var futurePosX = 0;
    var futurePosY = 0;
    switch(dir) {
        case "up":
            dy = -60;
            futurePosY = this.gridPos.y - 1;
            futurePosX = this.gridPos.x;
            break;
        case "down":
            dy = 60;
            futurePosY = this.gridPos.y + 1;
            futurePosX = this.gridPos.x;
            break;
        case "left":
            dx = -60;
            futurePosX = this.gridPos.x - 1;
            futurePosY = this.gridPos.y;
            break;
        case "right":
            dx = 60;
            futurePosX = this.gridPos.x + 1;
            futurePosY = this.gridPos.y;
            break;
    }
    var loc = this.game.tiling.tile(futurePosX,futurePosY);    	
    if (loc != 0 && loc != 4 && loc != 9 && loc != 15 && loc != 20 && loc != 13) {
        var offsetX = 0;
        var offsetY = 0;    

        if (dx < 0) {
            offsetY = 5;
            offsetX = -20;
        } else if (dx > 0) {
            offsetY = 5;
            offsetX = 40;
        }

        if (dy < 0) {
            offsetX = 5;
            offsetY = -20;
        } else if (dy > 0) {
            offsetX = 5;
            offsetY = 40;
        } 

        this.bonk.x = offsetX;
        this.bonk.y = offsetY;
        this.sprite.addChild(this.bonk);
        this.bonkSfx.play();
        this.bonk.animations.play("bonk", 5, false);


        return;
    }
    if (loc == 13) {
    	this.game.butPress();
    }
    this.gridPos.x = futurePosX;
    this.gridPos.y = futurePosY;

    this.walkSfx.play();
    this.target.x = this.sprite.x + dx;
    this.target.y = this.sprite.y + dy;

    this.sprite.animations.play("walk", 7, false);

    this.sprite.body.velocity.x = speed * dx / 60;
    this.sprite.body.velocity.y = speed * dy / 60;
}

Robot.prototype.update = function() {
    var commandDone = false;

    if (isNaN(this.target.x)) {
    	return this.done;
    }
    if (Math.abs(this.sprite.x - this.target.x) < 2.0 && Math.abs(this.sprite.y - this.target.y) < 2.0) {
        this.sprite.x = this.target.x;
        this.sprite.y = this.target.y;
        this.sprite.body.velocity.x = 0;
        this.sprite.body.velocity.y = 0;
        commandDone = true;
        this.target.x = NaN;
    }

    if (commandDone) {
        this.game.time.events.add(Phaser.Timer.SECOND, this.executeNext, this);
    }
    return this.done;
}

	
