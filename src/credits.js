var RoboCoop = RoboCoop || {};
var backButton;
RoboCoop.Credits = function(game) {

};

RoboCoop.Credits.prototype = {
	preload: function() {
		console.log('Credits - preload');
		this.load.spritesheet('back', 'gfx/back_nappi.png', 120, 78);
	},
	create: function() {
		console.log('Credits - create');
		backButton = this.add.button( 1024 - 185, 30, 'back', this.showMainMenu, this, 0, 0, 1); 
		text = this.add.text(this.world.centerX, this.world.centerY, "Mikko Lapinlahti\nRauli Puupera\nJanne Partala\nMikko Verhio\nTuure Vayrynen");
	    text.anchor.setTo(0.5);

	    text.font = 'Revalia';
	    text.fontSize = 60;

	    //  x0, y0 - x1, y1
	    grd = text.context.createLinearGradient(0, 0, 0, text.canvas.height);
	    grd.addColorStop(0, '#FF9900');   
	    grd.addColorStop(1, '#FFFFFF');
	    text.fill = grd;

	    text.align = 'center';
	    text.stroke = '#000000';
	    text.strokeThickness = 2;
	    text.setShadow(5, 5, 'rgba(0,0,0,0.5)', 5);

	    text.inputEnabled = true;
	    text.input.enableDrag();
	},
	showMainMenu: function() {
		this.state.start('MainMenu');	
	}
};
