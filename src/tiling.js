

Tiling = function() {
    this.level1 = [ 0, 0, 0, 0, 0,25,21,27, 0, 0,
                   0, 0, 0, 0, 0,24, 8,24, 0, 0,
                   0, 0, 0, 0, 0,31,20,32, 0, 0,
                   0, 0, 0, 0, 0,24, 0,24, 0, 0,
                  21,21,21,21,21, 2, 0,26,21,21,
                  15,15,15,15,15,29,15,29,15, 4,
                  21,21,21,28,21,22, 0,30,28,21,
                   0, 0, 0,24, 0, 0, 0, 0,24, 0,
                   0, 0, 0,24, 0, 9, 0, 0,24, 0,
                   0, 0, 0,24, 0, 0, 0, 6,24, 0];
    }

Tiling.prototype.weld = function(game, xPos, yPos) {
	var row = 0;
   	i = this.level1.indexOf(6);
   	j = xPos + (yPos*10);
   	console.log(i);
   	if (j == (i+1) || j == (i-1) || j == (i+10) || j == (i-10)) {
   		console.log("fixing");
   		i = this.level1.indexOf(6);
   		this.level1[i] = 7;
   		for (i; i>= 10; i = i - 10) {
			row += 1;
		}
   		tile = game.make.sprite((i * 60) + gridLeft, (row * 60) + gridTop, 'tileset1');
		tile.frame = 7;
        var fixed = game.add.sprite((i * 60) + gridLeft + 5, (row * 60) + gridTop, 'fixed');
        fixed.animations.add('fixed');
        fixed.animations.play('fixed', 5, false);
		this.batch.addChild(tile);

		row = 0;
		i = this.level1.indexOf(8);
		this.level1[i] = 13;
		for (i; i>= 10; i = i - 10) {
			row += 1;
		}
		tile = game.make.sprite((i * 60) + gridLeft, (row * 60) + gridTop, 'tileset1');
		tile.frame = 13;
		this.batch.addChild(tile);
   	}
}

Tiling.prototype.level = function(game, level, sb) {
	var col = 0;
	var row = 0;
	var i = 0;
	this.batch = sb;
	for (i = 0; i<this.level1.length; i++) {

    	tile = game.make.sprite((col * 60) + gridLeft, (row * 60) + gridTop, 'tileset1');

    	switch (this.level1[i]) {
    		case 0:
    			tile.frame = 18;
    		break;
    		case 24:
    			tile.frame = 21;
    			tile.x += 60; //anchorpoint on 0,0
    			tile.angle = 90;
    		break;
    		case 25:
    		    tile.frame = 2;
    		    tile.angle = 180;
    		    tile.x += 60;
    		    tile.y += 60;
    		break;
    		case 26:
    			tile.frame = 2;
    			tile.angle = 90;
    			tile.x += 60;
    		break;
    		case 27:
    			tile.frame = 2;
    			tile.angle = 270;
    			tile.y += 60;
    		break;
    		case 28:
    			tile.frame = 1;
    			tile.angle = 180;
    			tile.x += 60;
    			tile.y += 60;
    		break;
    		case 29:
    			tile.frame = 23;
    			tile.angle = 90;
    			tile.x += 60;
    		break;
    		case 30:
    			tile.frame = 22;
    			tile.angle = 180;
    			tile.x += 60;
    			tile.y += 60;
    		break;
    		case 31:
    			tile.frame = 1;
    			tile.angle = 90;
    			tile.x += 60;
    		break;
    		case 32:
	    		tile.frame = 1;
	    		tile.angle = 270;
	    		tile.y += 60;
    		break;
    		default:
    			tile.frame = this.level1[i];
    		break;
    	}
    	tile.z = 100;
		sb.addChild(tile);

		col++;
		if (col >= 10) {
			col = 0;
			row++;
		}
	}
}

Tiling.prototype.backGround = function(game, backG) {
	var col = 0;
	var row = 0;
	var i = 0;
	for (i = 0; i<100; i++) {
		tileB = game.make.sprite((col * 60) + gridLeft, (row * 60) + gridTop, 'tileset1');
		tileB.frame = 18;
		col++;
		if (col >= 10) {
			col = 0;
			row++;
		}
		backG.addChild(tileB);
	}
}

Tiling.prototype.steppedButton = function(game) {
	i = this.level1.indexOf(13);
	var row = 0;
	for (i; i>= 10; i = i - 10) {
		row += 1;
	}
	tile = game.make.sprite((i * 60) + gridLeft, (row * 60) + gridTop, 'tileset1');
	tile.frame = 5;
	this.batch.addChild(tile);

	while (this.level1.indexOf(29) != -1) {
		i = this.level1.indexOf(29);
		this.level1[i] = 15;
		row = 0;
		for (i; i>= 10; i = i - 10) {
			row += 1;
		}
		tile = game.make.sprite((i * 60) + gridLeft, (row * 60) + gridTop, 'tileset1');
		tile.frame = 15;
		this.batch.addChild(tile);
	}
	game.winning();
}

Tiling.prototype.tile = function(x,y) {
	var temp = x + (10*y);
	var num = this.level1[temp];
	return num;
}
