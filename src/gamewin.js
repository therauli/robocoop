var RoboCoop = RoboCoop || {};

RoboCoop.GameWin = function(game) {

};

RoboCoop.GameWin.prototype = {
	preload: function() {
		console.log('GameWin - preload');
		this.load.image('levelCompleted', 'gfx/level_completed.png');
		this.load.audio('victory', ['sfx/win_game.mp3', 'sfx/win_game.mp3',]);
	},
	create: function() {
		this.victory = this.add.audio('victory');
		this.add.sprite(200, 150, "levelCompleted"); 
		this.victory.play();
		this.time.events.add(Phaser.Timer.SECOND*6, this.startMainMenu, this);
	},
	startMainMenu: function() {
		this.state.start('MainMenu');	
	}
};
