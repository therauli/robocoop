var RoboCoop = RoboCoop || {};

RoboCoop.Story = function(game) {

};

RoboCoop.Story.prototype = {
	preload: function() {
		console.log('Story - preload');
		this.load.image('storyBoard', 'gfx/tarinakuva.png');
	},
	create: function() {
		this.add.sprite(0, 0, "storyBoard"); 
		this.time.events.add(Phaser.Timer.SECOND*10, this.startGame, this);
	},
	startGame: function() {
		this.state.start('Game');	
	}
};